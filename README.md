# Projet Complément Génie Logiciel (Docker)

Ce repository contient le fichier docker-compose.yml pour lancer l'ensemble des composants de l'application

## Exécution 

Pour exécuter l'application, utiliser la commande `docker-compose up -d`.
L'application frontend sera disponible sur le port 80 et l'application backend sur le port 8081 ([lien swagger](http://localhost:8081/swagger-ui/index.html)).

## DockerHub

- [Frontend](https://hub.docker.com/r/ossacipe/projetcomplementgenielogiciel-frontend)
- [Backend](https://hub.docker.com/r/ossacipe/projetcomplementgenielogiciel-backend)

## Code source

- [Frontend](https://gitlab.com/projet-compl-ment-g-nie-logiciel/frontend)
- [Backend](https://gitlab.com/projet-compl-ment-g-nie-logiciel/backend)

